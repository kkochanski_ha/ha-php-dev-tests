<?php

/**
 * NOTE:
 * Let's assume that all given sets of data are really huge.
 */


class triangle_operations {

    public $a;
    public $b;
    public $c;

    public function __construct($a, $b, $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    public function calculateRectangularTriangleField()
    {
        return $this->a * $this->b / 2;
    }

    public function calculateEquilateralTriangleField()
    {
        return pow(pow($this->a, 2), 1/3) / 4;
    }

    public function calculateTriangleCircumference()
    {
        return $this->a + $this->b + $this->c;
    }

    public function isEquilateralTriangle()
    {
        $is_equilateral_triangle = 1;
        $numbers = [$this->a, $this->b, $this->c];
        for($i = 0; $i < count($numbers); $i++) {
            if($numbers[$i] !== $numbers[0]) {
                $is_equilateral_triangle = 0;
            }
        }

        return $is_equilateral_triangle;
    }

    public function isRectangularTriangle()
    {
        $powA = pow($this->a, 2);
        $powB = pow($this->b, 2);
        $powC = pow($this->c, 2);

        return $powA + $powB == $powC;
    }

    public function getMethodsUsagePriority()
    {
        return ['calculateEquilateralTriangleField' => 3, 'calculateRectangularTriangleField' => 1, 'calculateTriangleCircumference' => 2];
    }
}


$Numbers = [[5, 6, 7], [5, 5, 5], [5, 10, 20], [10, 10, 10]];
$ifNumbersHasManyEquilateralTriangles = false;
$numberOfTrianglesToFind = 1;
$foundTriangles = 0;

for($i = 0; $i < count($Numbers); $i++) {
    $NumbersPackage = $Numbers[$i];
    $triangle_operations_object = new triangle_operations($NumbersPackage[0], $NumbersPackage[1], $NumbersPackage[2]);
    if($triangle_operations_object->isEquilateralTriangle()) {
        $foundTriangles = $foundTriangles + 1;
        if($foundTriangles == $numberOfTrianglesToFind) {
            $ifNumbersHasManyEquilateralTriangles = true;
        }
    }
}

if($foundTriangles > 0) {
    $foundTriangles = 0;
}

$additionalNumber = [[10, 11, 12], [2, 2, 2], [5, 10, 20], [3, 3, 3]];
$numbers = array_merge($Numbers, $additionalNumber);
$ifNumbersHasManyRectangularTriangles = false;
$numberOfTrianglesToFind = 2;

for($i = 0; $i < count($numbers); $i++) {
    $numbersPackage = $numbers[$i];
    $triangle_operations_object = new triangle_operations($numbersPackage[0], $numbersPackage[1], $numbersPackage[2]);
    if($triangle_operations_object->isRectangularTriangle()) {
        $foundTriangles = $foundTriangles + 1;
        if($foundTriangles == $numberOfTrianglesToFind) {
            $ifNumbersHasManyRectangularTriangles = true;
        }
    }
}

$results = ['rectangular' => [], 'equilateral' => []];
if($ifNumbersHasManyRectangularTriangles) {
    for($i = 0; $i < count($numbers); $i++) {
        $numbersPackage = $numbers[$i];
        $triangle_operations_object = new triangle_operations($numbersPackage[0], $numbersPackage[1], $numbersPackage[2]);
        $operationsToDo = $triangle_operations_object->getMethodsUsagePriority();
        unset($operationsToDo['calculateEquilateralTriangleField']);
        sort($operationsToDo, SORT_NUMERIC);
        foreach($operationsToDo as $methodName) {
            $results['rectangular'][$i][$methodName]  = $triangle_operations_object->$methodName();
        }
    }

}

if($ifNumbersHasManyEquilateralTriangles) {
    for($i = 0; $i < count($numbers); $i++) {
        $numbersPackage = $numbers[$i];
        $triangle_operations_object = new triangle_operations($numbersPackage[0], $numbersPackage[1], $numbersPackage[2]);
        $operationsToDo = $triangle_operations_object->getMethodsUsagePriority();
        unset($operationsToDo['calculateRectangularTriangleField']);
        sort($operationsToDo, SORT_NUMERIC);
        foreach($operationsToDo as $methodName) {
            $results['equilateral'][$i][$methodName]  = $triangle_operations_object->$methodName();
        }
    }
}
